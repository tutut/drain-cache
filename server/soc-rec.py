#!/opt/python3/bin/python3.5

"""A web.py application powered by gevent"""
import eventlet
from eventlet import wsgi
import http.client, os.path, json, mimetypes, string, time, shutil, sys, pwd, grp, optparse, signal, setproctitle, hashlib, multiprocessing, gc, glob, subprocess, queue, requests
from daemon import Daemon

eventlet.monkey_patch(socket=True, select=True, time=True)
response_status = queue.Queue()

root_dir = "/dev/shm/webcache"

def httpdate(seconds):
	return time.strftime("%a, %d %b %Y %H:%M:%S GMT",time.localtime(seconds))

def seconds(httpdate):
	try:
		sec = time.mktime(time.strptime(httpdate, "%a, %d %b %Y %H:%M:%S GMT"))
	except ValueError:
		return False
	return sec

class user_input:
        pass

def parseinput(userinput, **default):
        items = userinput.split('&')
        for item in items:
            if item == '': continue
            if item.find('=') == -1: item = item + '='
            key, value = item.strip().split('=')
            default[key.strip()] = value.strip()
        ui = user_input()
        for item in default:
                ui.__dict__[item] = default[item]
        return ui

http_code = { 200: "200 OK", 201: "201 Created", 204: "204 No Data", 205: "205 No Change", 400: "400 Bad Request", 404: "404 Not Found", 409: "409 Conflict", 500: "500 Internal Server Error" }

def error_status(response, code, description):
	response.status = code
	response.desc = description
	response.headers["Content-Type"] = "application/json"
	response.data.append(json.dumps({ "Error": description }))

def rparsing():
	while not response_status.empty():
		rminicapella = response_status.get()
	return rminicapella

""" directory => create. file => create/update (always ends with /) """
def PUT(request, response):
	http_response = {}
	path = request.path[1:]
	isdir = False
	if path.endswith('/'):
		realpath = os.path.join(root_dir, path[:-1])
		isdir = True
	else:
		realpath = os.path.join(root_dir, path)
	http_response["filename"] = realpath

	dirname = os.path.dirname(realpath)
	basename = os.path.basename(realpath)
	if not os.path.exists(dirname):
		if not 'force_create_dir' in request.qstrings or request.qstrings['force_create_dir'] != '1':
			try:
				os.makedirs(dirname)
			except Exception:
                                err = sys.exc_info()[1]
                                error_status(response, 500, "Cannot create " + dirname + ".")
                                response_status.put(500)
                                return

	if isdir:
		# create directory
		if os.path.exists(realpath):
			error_status(response, 409, "Directory " + basename + " already exists.")
			response_status.put(409)
			return

		if os.path.isdir(dirname):
			try:
                           os.mkdir(realpath)
			except Exception:
                           err = sys.exc_info()[1]
                           error_status(response, 500, "Cannot create " + realpath + ".")
                           response_status.put(500)
                           return

			response.headers["Content-Type"] = "application/json"
			response.data = [json.dumps(http_response)]
			response.status = 201
			response_status.put(response.status)
			return
		else:
			error_status(response, 409, basename + " is not a directory.")
			response_status.put(409)
			return

	else:
		# create/overwrite file
		response.status = None

		if os.path.isdir(realpath):
			error_status(response, 409, basename + " is a directory.")
			response.status = 409
			response_status.put(response.status)
			return
		mtime = 0
		if "wio-modification-time" in request.headers:
			mtime = seconds(request.headers["wio-modification-time"])
			if "wio-access-time" in request.headers:
                               	atime = seconds(request.headers["wio-access-time"])
			else: atime = mtime

		fd = None
		try:
			new_digest = hashlib.md5(request.data).hexdigest()
			http_response["md5"] = new_digest
			http_response["filesize"] = request.contentlen
			if os.path.exists(realpath):
				if not len(request.data):
					response.status = 204
					response_status.put(response.status)
					response.desc = "No Data"
					response.headers["Content-Type"] = "application/json"
					response.data = [json.dumps(http_response)]
					#return
				fd = open(realpath,'rb')
				digest = hashlib.md5()
				for line in fd:
					digest.update(line)
				fd.close()
				old_digest = digest.hexdigest()
				if old_digest == new_digest:
					response.status = 205
					response_status.put(response.status)
					response.desc = "No Change"
					response.headers["Content-Type"] = "application/json"
					response.data = [json.dumps(http_response)]
					#return
		except Exception:
                        err = sys.exc_info()[1]
                        if fd and not fd.closed: fd.close()

		try:
			fd = open(realpath+".~temp", "wb")
			fd.write(request.data)
			fd.close()

			m_beafore = hashlib.md5()
			fd_beafore = open(realpath+".~temp", "rb")
			for line in fd_beafore:
				m_beafore.update(line)
			fd_beafore.close()

			if http_response["md5"] == m_beafore.hexdigest():
				os.rename(realpath+".~temp", realpath)
				if mtime: os.utime(realpath, (atime, mtime))
			else:
				response_status.put(409)
				error_status(response, 409, "Error Create File (MD5).")
				response.headers["Content-Type"] = "application/json"
				response.data = [json.dumps(http_response)]
				#return


		except Exception:
			err = sys.exc_info()[1]
			response_status.put(500)
			if fd and not fd.closed: fd.close()
			error_status(response, 500, "Error Creating When Check MD5 in .~temp File.")
			return
		try:
			m = hashlib.md5()
			fd = open(realpath, "rb")
			for line in fd:
				m.update(line)
			fd.close()
			http_response["md5"] = m.hexdigest()
		except Exception:
			err = sys.exc_info()[1]
			response_status.put(500)
			if fd and not fd.closed: fd.close()
			error_status(response, 500, "Error GET (MD5) File.")

		path_file = realpath.rsplit('/', 1)[0]

		list_files = glob.glob(path_file+"/*.~temp")
		search_issolate = realpath+".~temp"

		if search_issolate in list_files:
			response.status = 409
			response_status.put(response.status)
			#os.remove(search_issolate)
			error_status(response, 409, "Error creating file issolate.")
			response.headers["Content-Type"] = "application/json"
			response.data = [json.dumps(http_response)]
			return
		else:
			response.status = 201
			response_status.put(response.status)
			response.headers["Content-Type"] = "application/json"
			response.data = [json.dumps(http_response)]
			return


""" get metadata of directory/file """
def HEAD(request, response):
	path = request.path[1:]
	realpath = os.path.join(root_dir, path)
	if not os.path.exists(realpath):
		error_status(response, 404, "Directory/file doesn't exists.")
		return

	try:
		stat = os.stat(realpath)
	except Exception:
                err = sys.exc_info()[1]
                error_status(response, 500, "Cannot stat file.")
                return

	if os.path.isfile(realpath):
		item = { "wio-size": stat.st_size,
		"wio-modification-time": httpdate(stat.st_mtime),
		"wio-access-time": httpdate(stat.st_atime) }
	else:
		item = { "wio-modification-time": httpdate(stat.st_mtime),
		"wio-access-time": httpdate(stat.st_atime) }

	for k in item:
		response.headers[k] = item[k]
	return

""" update metadata of directory/file """
def POST(request, response):
	path = request.path[1:]
	realpath = os.path.join(root_dir, path)

	userinput = parseinput(requests.utils.quote(request.data), operation='', destination='', modification_time='', access_time='')
	if userinput.operation == 'rename':
		if not os.path.exists(realpath):
			error_status(response, 404, "Directory/file doesn't exists.")
			return

		if not userinput.destination.startswith(request.domain + root_doc):
			error_status(response, 400, "Unknown destination.")
			return

		srcrealpath = realpath
		dstpath = os.path.relpath(userinput.destination, request.domain + root_doc)
		dstrealpath = os.path.join(root_dir, dstpath)

		if os.path.isdir(os.path.dirname(dstrealpath)):
			try:
				shutil.move(srcrealpath, dstrealpath)
			except Exception:
                                err = sys.exc_info()[1]
                                error_status(response, 500, "Cannot rename.")
                                return
		else:
			error_status(response, 400, "Cannot rename the file/directory. %s %s" %(dstpath, dstrealpath) )
			return
		return

	elif userinput.operation == 'change_attribute':
		if not os.path.exists(realpath):
			error_status(response, 404, "Directory/file doesn't exists.")
			return

		modtime = seconds(userinput.modification_time)
		accesstime = seconds(userinput.access_time)
		if accesstime or modtime:
			try:
				stat = os.stat(realpath)
				if not accesstime: accesstime = stat.st_atime
				if not modtime: modtime = stat.st_mtime
				os.utime(realpath, (accesstime, modtime))
			except Exception:
                                err = sys.exc_info()[1]
                                error_status(response, 500, "Error changing attribute.")
                                return
			return
		else:
			error_status(response, 400, "Attribute value not found.")
			return

	elif userinput.operation == 'symlink':
		dstrealpath = userinput.destination
		try:
			os.symlink(dstrealpath, realpath)
		except Exception:
                        err = sys.exc_info()[1]
                        error_status(response, 500, "Cannot symlink.")
                        return
		return
	else:
		error_status(response, 400, "Unknown operation.")
		return

class obj: pass

fd_log = None
fd_logdate = None
def log_request(remote_addr, strdate, method, url, req_length, ret_desc, response_time):
	global daemon, fd_log, fd_logdate
	now = time.strftime("%Y%m%d", time.localtime())
	if not fd_log or now != fd_logdate:
		fd_logdate = now
		if fd_log: fd_log.close()
		gc.collect()
		fd_log = open(daemon.options.stdout + "." + fd_logdate, "a+")

	fd_log.write("%s | %s | %s | %s | %s | %s | %.6f\n" % (remote_addr, strdate, method, url, req_length, ret_desc, response_time))
	fd_log.flush()

fd_logerr = None
fd_logerrdate = None
def log_error(host, method, path,err):
	global daemon, fd_logerr, fd_logerrdate
	now = time.strftime("%Y%m%d", time.localtime())
	if not fd_logerr or now != fd_logerrdate:
		fd_logerrdate = now
		if fd_logerr: fd_logerr.close()
		fd_logerr = open(os.path.dirname(daemon.options.stdout) + "/err-" + host + "." + fd_logerrdate, "a+")

	fd_logerr.write("%s | %s | %s | %s\n" % (time.strftime("%d/%m/%Y %H:%M:%S",time.localtime()), method, path, err))
	fd_logerr.flush()

def fix_destination(hostname, request):
	userinput = parseinput(requests.utils.quote(request.data), operation='', destination='', modification_time='', access_time='')
	if userinput.operation in ['rename']:
		dest = http.client.urlsplit(userinput.destination)
		userinput.destination = "http://%s%s" % (hostname, dest.path)
		request.data = ''
		for item in userinput.__dict__:
			if userinput.__dict__[item] == '': continue
			request.data += "%s=%s&" % (item, userinput.__dict__[item])
		request.data = requests.utils.quote(request.data)


def wsgi_func(env, start_response):
                global hosts, reject_urls

                start_time = time.time()
                request = obj()
                response = obj()
                request.path = requests.utils.unquote(env.get('PATH_INFO'))
                request.headers = {}

                for key in env:
                   if key.startswith('HTTP_') and key != 'HTTP_HOST':
                      request.headers[key[5:].lower().replace('_','-')] = env[key]
                request.domain = "http://" + env.get('HTTP_HOST', '[unknown]')
                request.qstrings = {}
                items = requests.utils.unquote(env.get('QUERY_STRING','')).split('&')
                for item in items:
                    if item == '': continue
                    if item.find('=') == -1:
                        item = item + '='
                        key, value = item.strip().split('=')
                        request.qstrings[key.strip()] = value.strip()
                request.contentlen = int(env.get('CONTENT_LENGTH', '0'))
                request.data = env['wsgi.input'].read(request.contentlen)
                method = env.get('REQUEST_METHOD')
                request.method = method
                response.status = 200
                response.headers = {}
                response.data = []

                rejected = False
                for url in reject_urls:
                    if request.path.startswith(url): rejected = True

                if rejected:
                    start_response('205 No Change', [('Content-Type', 'text/plain')])
                    return ['URL is rejected from list !!\r\n']
                elif method in globals():
                    if not daemon.options.readonly or request.method in ['GET', 'HEAD']:
                          globals()[method](request, response)
                    else:
                          response.status = 200
                          response.headers['Content-Type'] = 'text/plain'

                    start_response('%s' % (http_code[response.status]),response.headers.items())

                    force_write = True
                    if 'force' in request.qstrings and request.qstrings['force'] == '1' and response.status in [205, 409]:
                       force_write = True

                    if method == 'PUT':
                        rminicapella = rparsing()
                        if rminicapella == 201 or rminicapella == 200 or rminicapella == 204 or rminicapella == 205:
                               if request.method in ['PUT'] and (response.status in [200, 201] or force_write):
                                     for host in hosts: hosts[host]['queue'].put(request)
                        elif rminicapella == 409 or rminicapella == 500:
                               pass
                    elif method == 'POST' or method == 'DELETE':
                        if request.method in ['POST', 'DELETE'] and (response.status in [200, 201] or force_write) :
                            for host in hosts: hosts[host]['queue'].put(request)

                    if 'desc' in response.__dict__:
                        desc = response.desc
                    else: desc = "OK"
                    log_request(env.get('REMOTE_ADDR'), \
                        time.strftime("%d/%m/%Y %H:%M:%S",time.localtime()), \
                        method, request.path, request.contentlen, desc, time.time() - start_time)

                    return response.data
                else:
                    start_response('404 Not Found', [('Content-Type', 'text/plain')])
                    return ['Method is not Implemented !!\r\n']

is_exit = multiprocessing.Value('b', 0)
def clean_exit(signum, frame):
        global hosts
        is_exit.value = 1
        for host in hosts:
            hosts[host]['proc'].join()
        sys.exit()

def do_gc(signum, frame):
        gc.collect()

max_retry = 2
def http_request(host, req):
	get_http = http.client.HTTPConnection(host, timeout=5)
	success = False
	retry = 0
	if req.method == 'POST': fix_destination(host, req)

	while not success and retry <= max_retry:
		try:
			get_http.request(req.method, requests.utils.quote(req.path), req.data, req.headers)
			resp = get_http.getresponse()
			if resp.status == 404 and req.method == 'PUT':
				get_http.request(req.method, requests.utils.quote(req.path) + "?force_create_dir=1", req.data, req.headers)
				resp = get_http.getresponse()

			if resp.status != 500 or resp.status != 409:
				success = True
			else: retry += 1
		except Exception:
			err = sys.exc_info()[0]
			success = False
			retry += 1
			get_http.close()
	if not success:
		log_error(host, req.method, req.path, err)


def replicator(host, queue):
	setproctitle.setproctitle(sys.argv[0] + ' %s' % host)
	pool = eventlet.GreenPool()
	while not is_exit.value:
		if not queue.empty():
			request = queue.get()

			if request.method == 'DELETE' or (request.method == 'POST' and requests.utils.quote(request.data).find('operation=rename') >= 0) or request.path.endswith('/'):
				# make it sequential for rename or operation on directory
				http_request(host, request)
			else:
				pool.spawn_n(http_request, host, request)
		else:
                        pass
			#eventlet.sleep(.2)

daemon = Daemon(
	user="{user-running-apps}",
	group="{group-running-apps}",
        stdin="/dev/null",
        stdout="/var/log/webio/drain_cache.log",
        stderr="/var/log/webio/drain_cache.err",
        pidfile="/var/log/webio/drain_cache.pid",
	port="40405",
	rootdir=root_dir,
	readonly=False,
)

hosts = {}
reject_urls = {}
if __name__ == "__main__":
	if daemon.service():
		values = 22
		signal.signal(signal.SIGABRT, clean_exit)
		signal.signal(signal.SIGTERM, clean_exit)
		signal.signal(signal.SIGQUIT, clean_exit)
		signal.signal(signal.SIGINT, clean_exit)
		signal.signal(signal.SIGUSR1, do_gc)
		setproctitle.setproctitle(sys.argv[0])
		os.umask(values)
		os.chdir("/opt/python3/apps/drain_cache/server")

		host_list = map(lambda x: x.strip(), open("host_list.conf", "r").readlines())
		for hostname in host_list:
			queue = multiprocessing.Queue()
			hosts[hostname] = { 'queue': queue ,'proc': multiprocessing.Process(target=replicator, args=(hostname, queue)) }
			hosts[hostname]['proc'].start()

		if os.path.exists("reject.conf"):
			reject_urls = map(lambda x: x.strip(), open("reject.conf", "r").readlines())

		port = 40405 if not daemon.options.port.isdigit() else int(daemon.options.port)
		root_dir = root_dir if root_dir == daemon.options.rootdir else daemon.options.rootdir
		sys.stdout.write("Using port: %s and root directory: %s\n" % (port,root_dir))
		sys.stdout.flush()
		wsgi.server(eventlet.listen(('', port)), wsgi_func, open("/dev/null","w"), keepalive=False)
