import sys, signal, os, time, multiprocessing, setproctitle

from smtplib import SMTP
from subprocess import Popen
from subprocess import PIPE
from requests import put
from requests import get
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from email.mime.text import MIMEText
from daemon import Daemon

queue = multiprocessing.Queue()
dicts = []

ROOT_DIR = "{work-dir}"
DIR_WATCH = "{cache-dir}"
DEST_ADDR = "{ip-destination}:{port-destination}"

fd_logerr = None
fd_logerrdate = None
def log_error(host,path):
	global daemon, fd_logerr, fd_logerrdate
	now = time.strftime("%Y%m%d", time.localtime())
	if not fd_logerr or now != fd_logerrdate:
		fd_logerrdate = now
		if fd_logerr: fd_logerr.close()
		fd_logerr = open(os.path.dirname(daemon.options.stdout) + "/err-" + host + "." + fd_logerrdate, "a+")

	fd_logerr.write("%s | %s\n" % (time.strftime("%d/%m/%Y %H:%M:%S",time.localtime()), path))
	fd_logerr.flush()

is_exit = multiprocessing.Value('b', 0)
def clean_exit(signum, frame):
        global p
        is_exit.value = 1
        for pr in p:
            p[smart_soc]['proc'].join()
        #sys.exit()

def do_gc(signum, frame):
        gc.collect()

class Progress:
    @classmethod
    def ts(self,parser):
        try:
           codes = get('http://'+DEST_ADDR+parser.replace(DIR_WATCH,""))
           resp_code = codes.status_code
        except Exception:
           resp_code = 502
           log_error(DEST_ADDR,parser)

        if resp_code == 200 or resp_code == 502:
            print("File Existing")
            sys.stdout.flush()
        else:
           try:
                  data = open(parser, 'rb').read()
                  res = put(url='http://'+DEST_ADDR+parser.replace(DIR_WATCH,""),data=data,headers={'Content-Type': 'application/octet-stream'})
                  status_code = res.status_code
           except Exception:
               status_code = 502
               log_error(DEST_ADDR,parser)

class Watcher:
    def __init__(self):
        self.observer = Observer()

    def run(self,soc):
        event_handler = Handler()
        self.observer.schedule(event_handler, soc, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(0)
        except:
            self.observer.stop()
            print("Error Exited")
            sys.stdout.flush()

        self.observer.join()


class Handler(FileSystemEventHandler):
    @staticmethod
    def on_any_event(event):
        event_type = event.event_type
        if event.is_directory:
            return None

        elif event_type == 'created':
            Progress.ts(event.src_path.rsplit('.', 1)[0])

        elif event.event_type == 'deleted':
            return None
            # Taken any action here when a file is modified.
            # print("Received deleted event - %s." % event.src_path)
            # sys.stdout.flush()

        else:
             return None

def handle_queue(queue,smart_soc):
    setproctitle.setproctitle(sys.argv[0] + ' %s' % smart_soc)
    w = Watcher()
    while not queue.empty():
        soc = queue.get()
        w.run(soc)

daemon = Daemon(
	user="{user-running-apps}",
	group="{group-running-apps}",
        stdin="/dev/null",
        stdout="/var/log/drain_cache/drain.log",
        stderr="/var/log/drain_cache/drain.err",
        pidfile=ROOT_DIR+"/drain_cache.pid",
	port="40404",
        procname=sys.argv[0],
	rootdir=ROOT_DIR,
	readonly=False,
)

"""
Noted:

Jalanakan nginx dahulu baru service drain cache
Jika ada perubahan skema cache di sisi nginx lebih baik diatikan dulu

"""

p = {}
if __name__ == '__main__':
    if daemon.service():
       values = 22
       signal.signal(signal.SIGABRT, clean_exit)
       signal.signal(signal.SIGTERM, clean_exit)
       signal.signal(signal.SIGQUIT, clean_exit)
       signal.signal(signal.SIGINT, clean_exit)
       signal.signal(signal.SIGUSR1, do_gc)
       setproctitle.setproctitle(sys.argv[0])
       os.umask(values)
       os.chdir(ROOT_DIR)


       soc = [os.path.join(DIR_WATCH,o) for o in os.listdir(DIR_WATCH) if os.path.isdir(os.path.join(DIR_WATCH,o))]
       for smart_soc in range(len(soc)):
           queue.put(soc[smart_soc])
           p[smart_soc] = {'queue_num': soc[smart_soc] ,'proc': multiprocessing.Process(target=handle_queue, args=(queue,soc[smart_soc]))}
           p[smart_soc]['proc'].start()
